class Game
  attr_accessor :player_one, :player_two, :player_one_selection, :player_two_selection, :player_one_arr, :player_two_arr

    WINING_POSITIONS = [[0, 1, 2], [3, 4, 5], [6, 7, 8],
    [0, 3, 6], [1, 4, 7], [2, 5, 8],
    [0, 4, 8], [2, 4, 6]]

    def initialize()

        
        @arr= Array.new(9)
        @switch=false
        @player_one_arr=[]
        @player_two_arr=[]
        @result=[]
    end

    def choose_players()
        
      puts "what is the first player name?"
      player_one = gets.chomp
      puts "what do you want X/O?"   
      player_one_selection = gets.chomp.upcase

    while player_one_selection != "X" or player_one_selection != "O" do

        break if player_one_selection=="X" or player_one_selection=="O"
        puts "plz select the right tool"
        puts "what do you want X/O?"
        player_one_selection = gets.chomp.upcase

    end

    @player_one_arr= [player_one, player_one_selection]

    puts "what is the second player name?"
    player_two = gets.chomp

    if player_one_selection=="X" 
        player_two_selection ="O"
    elsif player_one_selection=="O"
        player_two_selection="X"
    end

    @player_two_arr= [player_two,player_two_selection]
    puts "the first player is #{player_one} with #{player_one_selection} and the second player is #{player_two} with #{player_two_selection}"

    end

    def update_board(arr)
        
        puts "#{@arr[0]} |#{@arr[1]} |#{@arr[2]}"

        puts "#{@arr[3]} |#{@arr[4]} |#{@arr[5]}"

        puts "#{@arr[6]} |#{@arr[7]} |#{@arr[8]}"
        
    end 
      
    def check_for_winner(arr)
        WINING_POSITIONS.each do |i , j , k|
            if arr[i]==arr[j] && arr[i]==arr[k] && arr[i]=="X"  
                @result<<"X"
                return true
            elsif arr[i]==arr[j] && arr[i]==arr[k] && arr[i]=="O"
                puts "O wins"
                @result<<"O"
                return true
            end
        end        
        false
     
    end

    def announce_winner()
        if @result & @player_one_arr
           puts "the winner is #{@player_one_arr.first} with #{@player_one_arr.last}"
        else
            puts "the winner is #{@player_two_arr.first} with #{@player_two_arr.last}"
        end
        
    end

    def inputFromUsers()
        if !(@switch)
            player_selection= @player_one_arr.last
            puts "it is #{@player_one_arr.first} turn...where do you wanna put it?"
            player_position= gets.chomp.to_i

            until player_position.between?(0,8) do
                puts "choose in the range plz (0..8)" 
                player_position= gets.chomp.to_i
            end

            while @arr.at(player_position)  do
                break if !(@arr.at(player_position))
                puts "this seat is taken choose another one"
                player_position= gets.chomp.to_i
            end
          
            @arr[player_position]=player_selection
            @switch= true

        elsif @switch
            player_selection= @player_two_arr.last
            puts "it is #{player_two_arr.first} turn...where do you wanna put it?"
            player_position= gets.chomp.to_i
            
            until player_position.between?(0,8) do
                puts "choose in the range plz (0..8)" 
                player_position= gets.chomp.to_i
            end
            while @arr.at(player_position)  do
                break if !(@arr.at(player_position))
                puts "this seat is taken choose another one"
                player_position= gets.chomp.to_i
            end
          
            @arr[player_position]=player_selection
            @switch=false
        end

    end

    def startGame()
        self.choose_players()
        #this is going to be while loop
        while @arr.include? (nil) do
            self.inputFromUsers
            self.update_board(@arr)
            if self.check_for_winner(@arr)
                self.announce_winner()
                break
            end
             if !(@arr.include? (nil)) && !(self.check_for_winner(@arr))
                puts "it's a draw"
                break
             end
        end
        
    end
end

besmella= Game.new
besmella.startGame



